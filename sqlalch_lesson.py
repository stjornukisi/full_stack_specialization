"""
Explanation of SQLAlchemy vs. Flask-SQLAlchemy usage.
"""

# ------------------------------------------------------------------
# SECTION 1: Defining Models
# ------------------------------------------------------------------

# SQLAlchemy uses declarative_base to create the base class for all models.
# When a model class (table definition) inherits from this base class, it is automatically registered
# within Base.metadata. This allows SQLAlchemy to track all table definitions and manage their creation.

# --- For a plain SQLAlchemy setup ---
# 1. Define the base class for models:
from sqlalchemy.orm import DeclarativeMeta
from sqlalchemy.ext.declarative import declarative_base
Base: DeclarativeMeta = declarative_base()

# 2. Define the model:
from sqlalchemy import Column, String
class MySQLAlchemyModel(Base):
    __tablename__ = 'my_model'  # Explicit table name
    column1 = Column(String(64), primary_key=True)
    column2 = Column(String(64))

# --- For a Flask-SQLAlchemy app ---
# 1. Initialize Flask-SQLAlchemy:
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

# 2. Define the model:
class MyFlaskSQLAlchemyModel(db.Model):
    __tablename__ = 'my_flask_model'  # Explicit table name
    column1 = db.Column(db.String(64), primary_key=True)
    column2 = db.Column(db.String(64))

# ------------------------------------------------------------------
# SECTION 2: Creating Tables
# ------------------------------------------------------------------

# After defining the models, SQLAlchemy needs an engine to create tables.
# The engine is the connection to the database.

# --- For a plain SQLAlchemy setup ---
# 1. Create a plain SQLAlchemy engine:
from sqlalchemy import create_engine
plain_sqlalchemy_engine = create_engine('your-database-url')  # Example: 'sqlite:///plain_database.db'

# 2. Create tables:
Base.metadata.create_all(bind=plain_sqlalchemy_engine)

# --- For a Flask-SQLAlchemy app ---
from flask import Flask
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///flask_database.db'  # Example database URI
db.init_app(app)  # Bind Flask app to Flask-SQLAlchemy

# 1. Create tables within the app context:
# Flask requires an application context to access resources like the database connection. 
# The context provides access to objects that are tied to the current app, like `db`.
with app.app_context():
    # It's typical to import models and routes here to ensure that they are registered with Flask.
    # Importing routes and models ensures that any database models or route handlers are registered
    # before the app starts. Without this, tables defined in models may not be recognized.
    # In larger apps, this also prevents circular imports by delaying these imports.
    # Example:
    # from . import routes, models
    db.create_all()  # Ensures all models inheriting from db.Model are registered

# 2. Retrieve the Flask-SQLAlchemy engine (if necessary):
# Flask-SQLAlchemy abstracts the engine, so you rarely need to access it directly. 
# However, if needed, you can retrieve the engine like this:
flask_sqlalchemy_engine = db.get_engine()

# ------------------------------------------------------------------
# SECTION 3: Working with Sessions
# ------------------------------------------------------------------

# Interacting with the database requires a session for adding, querying, updating, or deleting records.

# --- For a plain SQLAlchemy setup ---
# 1. Create a configured "Session" class for the plain SQLAlchemy engine:
from sqlalchemy.orm import sessionmaker
PlainSession = sessionmaker(bind=plain_sqlalchemy_engine)

# 2. Create a session instance:
plain_session = PlainSession()

# 3. Add a new record:
new_record_plain = MySQLAlchemyModel(column1='value1', column2='value2')
plain_session.add(new_record_plain)
plain_session.commit()

# 4. Query the database:
plain_records = plain_session.query(MySQLAlchemyModel).all()  # Retrieve all records
plain_record = plain_session.query(MySQLAlchemyModel).filter_by(column1='value1').first()  # Retrieve a specific record

# 5. Update a record:
plain_record = plain_session.query(MySQLAlchemyModel).get('record_id')
if plain_record:
    plain_record.column1 = 'new_value'
    plain_session.commit()

# 6. Delete a record:
plain_session.delete(plain_record)
plain_session.commit()

# --- For a Flask-SQLAlchemy app ---
# Flask-SQLAlchemy manages the session through `db.session` scoped to the Flask application context.
#
# Flask-SQLAlchemy automatically handles session management, meaning:
# - The session (`db.session`) is tied to the application context, which ensures that each request gets its own session.
# - It simplifies working with the database because you don't have to manually manage the session's lifecycle.
# - When the request is finished, the session is automatically committed (or rolled back if there was an error) and closed.
# 
# Scoping:
# - The session is *scoped* to the request, meaning each incoming HTTP request gets its own session.
# - This ensures that changes made to the database during a request are isolated and safely committed once the request is complete.

# 1. Add a new record:
new_record_flask = MyFlaskSQLAlchemyModel(column1='value1', column2='value2')  # type: ignore (Pylance might not detect dynamic fields)
db.session.add(new_record_flask)
db.session.commit()

# 2. Query the database:
flask_records = MyFlaskSQLAlchemyModel.query.all()  # Retrieve all records
flask_record = MyFlaskSQLAlchemyModel.query.filter_by(column1='value1').first()  # Retrieve a specific record

# 3. Update a record:
flask_record = MyFlaskSQLAlchemyModel.query.get('record_id')
if flask_record:
    flask_record.column1 = 'new_value'
    db.session.commit()

# 4. Delete a record:
db.session.delete(flask_record)
db.session.commit()

# ------------------------------------------------------------------
# SECTION 4: Key Concepts
# ------------------------------------------------------------------

# - Engine: The connection to the database, responsible for executing SQL commands.
# - Session:
#   - In plain SQLAlchemy: A manually managed transactional workspace used for querying, adding, updating, or
#     deleting objects mapped to database tables.
#   - In Flask-SQLAlchemy: Managed automatically by Flask's application context and scoped to handle requests
#     properly. Use `db.session` for all database interactions within a Flask app.
#     Flask-SQLAlchemy automatically manages the lifecycle of the session.
